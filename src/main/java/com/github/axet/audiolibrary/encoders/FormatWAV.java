package com.github.axet.audiolibrary.encoders;

// based on http://soundfile.sapp.org/doc/WaveFormat/

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public class FormatWAV implements Encoder {
    int NumSamples;
    EncoderInfo info;
    int BytesPerSample;
    RandomAccessFile outFile;

    public static ByteOrder ORDER = ByteOrder.LITTLE_ENDIAN;
    public static int SHORT_BYTES = Short.SIZE / Byte.SIZE;

    public FormatWAV(EncoderInfo info, File out) {
        this.info = info;
        NumSamples = 0;

        BytesPerSample = info.bps / 8;

        try {
            outFile = new RandomAccessFile(out, "rw");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        save();
    }

    public void save() {
        int SubChunk1Size = 16;
        int SubChunk2Size = NumSamples * info.channels * BytesPerSample;
        int ChunkSize = 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size);

        write("RIFF", ByteOrder.BIG_ENDIAN);
        write(ChunkSize, ORDER);
        write("WAVE", ByteOrder.BIG_ENDIAN);

        int ByteRate = info.hz * info.channels * BytesPerSample;
        short AudioFormat = 1; // PCM = 1 (i.e. Linear quantization)
        int BlockAlign = BytesPerSample * info.channels;

        write("fmt ", ByteOrder.BIG_ENDIAN);
        write(SubChunk1Size, ORDER);
        write((short) AudioFormat, ORDER); //short
        write((short) info.channels, ORDER); // short
        write(info.hz, ORDER);
        write(ByteRate, ORDER);
        write((short) BlockAlign, ORDER); // short
        write((short) info.bps, ORDER); // short

        write("data", ByteOrder.BIG_ENDIAN);
        write(SubChunk2Size, ORDER);
    }

    void write(String str, ByteOrder order) {
        try {
            byte[] cc = str.getBytes("UTF-8");
            ByteBuffer bb = ByteBuffer.allocate(cc.length);
            bb.order(order);
            bb.put(cc);
            bb.flip();
            outFile.write(bb.array());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void write(int i, ByteOrder order) {
        ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
        bb.order(order);
        bb.putInt(i);
        bb.flip();
        try {
            outFile.write(bb.array());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void write(short i, ByteOrder order) {
        ByteBuffer bb = ByteBuffer.allocate(SHORT_BYTES);
        bb.order(order);
        bb.putShort(i);
        bb.flip();
        try {
            outFile.write(bb.array());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void encode(short[] buf, int pos, int buflen) {
        NumSamples += buflen / info.channels;
        try {
            ByteBuffer bb = ByteBuffer.allocate(buflen * SHORT_BYTES);
            bb.order(ORDER);
            bb.asShortBuffer().put(buf, pos, buflen);
            bb.flip();
            outFile.write(bb.array());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void end() {
        try {
            outFile.seek(0);
            save();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            end();
            outFile.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public EncoderInfo getInfo() {
        return info;
    }
}
